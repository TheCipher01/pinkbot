const Discord = require("discord.js");
    
module.exports.run = async(bot, message, args) => {
    
    if(!message.member.hasPermission("ADMINISTRATOR")) return message.reply("You do not have permission to run this command!");
	if(!args[0]) return message.channel.send("Usage: >>clear <# of messages>");
	message.channel.bulkDelete(args[0]).then(() => {
	    message.channel.send(`Cleared ${args[0]} messages.`).then(msg => msg.delete(5000));	
	});
}
	
	
module.exports.help = {
    name: "clear"
}